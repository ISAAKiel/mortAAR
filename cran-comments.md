## Possibly mis-spelled words in DESCRIPTION
"indices" and "survivorship" are correct.

## Increment patch
This is a minor increment. In this version we have:
- removed references in the vignettes to csl-file as this caused an error
- therefore, also the suggestion of RCurl has been removed

## Test environments
* local OS X install, R 4.0.3
* ubuntu 20.04 (on github-action), R 4.2.0
* win-builder (devel and release)

## R CMD check results
There were no ERRORs or WARNINGs.
There was one NOTE on "checking CRAN incoming feasibility".

## Reverse dependencies
There are no reverse dependencies.
